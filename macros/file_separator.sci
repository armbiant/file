// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function s = file_separator (  )
  // Return the native separator for the current platform
  // 
  // Calling Sequence
  //
  // Parameters
  // s : a 1-by-1 matrix of strings.
  //
  // Description
  //  Return the native separator for the current platform
  // The separator depends on the platform :
  // <itemizedlist>
  // <listitem>"/" on Unix, Linux systems,</listitem>
  // <listitem>"\" on Windows systems,</listitem>
  // <listitem>":" on Macintosh.</listitem>
  // </itemizedlist>
  //
  // Examples
  // file_separator()
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  platform = getos ()
  if ( platform == "Windows" ) then
    s = "\"
  elseif ( platform == "Linux" ) then
    s = "/"
  else
    s = ":"
  end
endfunction

