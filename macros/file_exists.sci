// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt





function ex = file_exists ( name )
  // Returns %t if the file exists
  // 
  // Calling Sequence
  // ex = file_exists ( name )
  //
  // Parameters
  // ex : a 1-by-1 matrix of booleans, ex is %t if the file exists, ex is %f if not
  //
  // Description
  // Returns %t if the file exists.
  // 
  // Examples
  // name = file_tempfile ( )
  // file_touch ( name )
  // ex = file_exists ( name ) // %t
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  ex = (fileinfo(name)<>[])
endfunction

