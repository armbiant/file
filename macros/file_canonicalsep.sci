// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt





function sep = file_canonicalsep ( )
  // Returns the platform-dependent canonical separator.
  // 
  // Calling Sequence
  // sep = file_canonicalsep ( )
  //
  // Parameters
  // sep : a 1-by-1 matrix of strings, sep="/" on Windows and Linux, sep=":" on Mac
  //
  // Description
  // Returns the platform-dependent canonical separator.
  // 
  // Examples
  // sep = file_canonicalsep ( )
  // 
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  // Copyright (c) 2008 - Michael Baudin
  // Copyright (c) 2008 - Arjen Markus
  
  os = getos()
  if ( os=="Windows" ) then
    sep = "/"
  elseif ( os=="Linux" ) then
    sep = "/"
  else
    sep = ":"
  end
endfunction

