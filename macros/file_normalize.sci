// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function path = file_normalize ( name )
    // Returns a unique normalized path.
    // 
    // Calling Sequence
    // name : a 1-by-1 matrix of strings
    // path : a 1-by-1 matrix of strings
    //
    // Description
    // Returns a unique normalized path representation for the 
    // file-system object (file, directory, link, etc), whose string 
    // value can be used as a unique identifier for it.
    // A normalized path
    // is an absolute path which has all '../', './' removed. Also it is one which
    // is in the ``standard'' format for the native platform.
    // On Windows or Mac, any platform-specific separator in the path
    // is replaced by the platform-independent separator "/".
    // On Windows it also means we want the long form with that form's
    // case-dependence (which gives us a unique, case-dependent path).
    //
    // Examples
    //   file_normalize ( "/foo/myfile.txt" )
    //   expected = "/foo/bla/myfile.txt";
    //   //
    //   file_normalize ( "\foo\myfile.txt" )
    //   expected_Windows = "/foo/bla/myfile.txt"
    //   expected_Linux = pwd()+"/\foo\bla\myfile.txt"
    //   //
    //   file_normalize ( "." )
    //   expected = pwd()
    //   //
    //   file_normalize ( ".." )
    //   //
    //   file_normalize ( "../file.txt" )
    // 
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (c) 2008 - Michael Baudin
    // Copyright (c) 2008 - Arjen Markus

    //
    // Compute an absolute path name : absolutename
    pathtype = file_pathtype ( name )
    if ( pathtype == 1 ) then
        absolutename = name
    elseif ( pathtype == 3 ) then
        //
        // Replace the leading / by the current volume name
        //
        cwd = pwd ()
        firstsep = file_firstsepindex ( cwd )
        filevolume = part ( cwd , 1:firstsep - 1 )
        // Remove the leading / in the filename
        filelength = length ( name )
        if ( filelength > 1 ) then
            fileend = part ( name , 2 : filelength )
            absolutename = file_join ( filevolume , fileend )
        else
            absolutename = filevolume
        end
    else
        //
        // Add the current directory to the relative path
        //
        directory = pwd ()
        absolutename = file_join ( [directory , name] )
    end 
    //
    // 2. Splitting the absolutename into pieces
    //
    listOfFiles = file_split ( absolutename )
    nbitems = size ( listOfFiles , "*" )
    //
    // 3. Tag the components in the path which must be kept
    //
    // By default all items must be kept in the path
    keepitem(1:nbitems) = %t
    // skipnb is the current number of items to skip
    skipnb = 0
    //
    // 3.1 Process a loop from the end of the path to the begining
    // Compute keepitem(i): %t if i is to keep, %f if to remove
    //
    for i = nbitems : -1 : 1
       item = listOfFiles ( i )
       if ( item == ".." ) then
          // If the item is a "..", then remember that there is a path to skip and skip the ".." itself
          skipnb = skipnb + 1
          keepitem ( i ) = %f
       elseif ( item == "." ) then
          keepitem ( i ) = %f
       elseif ( skipnb > 0 ) then
          // If the current item is a regular path, but there is an item to skip
          // (because of a previous ".."), skip it
          skipnb = skipnb - 1
          keepitem ( i ) = %f
       end
    end
    //
    // 4. Now compute the normalized path by keeping only the required
    // path components and insert separator between components, if necessary
    //
    path = ""
    separator = file_canonicalsep ( )
    pathindex = 0
    for i = 1 : nbitems
       if ( keepitem ( i ) ) then
          pathindex = pathindex + 1
          item = listOfFiles ( i )
          if ( pathindex > 1 & path <> separator ) then
             path = path + separator
          end
          path = path + item
       end
    end
    //
    // 5. If the given file is a directory, the name must end with a "/"
    //
    if ( %f ) then
    // Surely, not on Linux. On Windows ?
    isdirectory = file_isdirectory ( path )
    if ( isdirectory ) then
       separator = file_canonicalsep ()
       path = path + separator
    end
    end
endfunction

