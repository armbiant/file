//
// This help file was automatically generated from file_split.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_split.sci
//

name = file_tempfile (  )
pathmat = file_split ( name )
//
pathmat = file_split ( "/foo/myfile.txt" )
expected = ["/";"foo";"myfile.txt"]
//
pathmat = file_split ( "C:\foo\myfile.txt" )
expected_Windows = ["C:\";"foo";"myfile.txt"]
expected_Linux = "C:\foo\myfile.txt"
//
pathmat = file_split ( "C:/foo/myfile.txt" )
expected_Windows = ["C:/";"foo";"myfile.txt"]
expected_Linux = ["C:";"foo";"myfile.txt"]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_split.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
