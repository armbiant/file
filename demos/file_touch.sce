//
// This help file was automatically generated from file_touch.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_touch.sci
//

name = file_tempfile (  )
file_touch ( name )
x = fileinfo ( name );
mprintf("Last modification: %d\n",x(6))
//
// Wait for 1 second
realtimeinit(1);//sets time unit to half a second
realtime(0);//sets current date to 0
realtime(1);
//
// Touch it again
file_touch ( name )
x = fileinfo ( name );
mprintf("Last modification: %d\n",x(6))
halt()   // Press return to continue
 
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_touch.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
