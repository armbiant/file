//
// This help file was automatically generated from file_exists.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of file_exists.sci
//

name = file_tempfile ( )
file_touch ( name )
ex = file_exists ( name ) // %t
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "file_exists.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
