// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if ( and ( computed==expected ) ) then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

name = file_tempfile (  );
pathmat = file_split ( name );
assert_equal ( typeof(pathmat) , "string" );
assert_equal ( size(pathmat,"c") , 1 );
assert_equal ( size(pathmat,"r")>1 , %t );
//
pathmat = file_split ( "/foo/myfile.txt" );
expected = ["/";"foo";"myfile.txt"];
assert_equal ( pathmat , expected );
//
pathmat = file_split ( "C:\foo\myfile.txt" );
if ( getos()=="Windows" ) then
  assert_equal ( pathmat , ["C:\";"foo";"myfile.txt"] );
else
  assert_equal ( pathmat , "C:\foo\myfile.txt" );
end
//
pathmat = file_split ( "C:/foo/myfile.txt" );
if ( getos()=="Windows" ) then
  assert_equal ( pathmat , ["C:/";"foo";"myfile.txt"] );
else
  assert_equal ( pathmat , ["C:";"foo";"myfile.txt"] );
end



